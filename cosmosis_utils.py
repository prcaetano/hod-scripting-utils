import os
import numpy as np
from cosmosis.runtime.config import Inifile
from cosmosis.runtime.pipeline import LikelihoodPipeline, Pipeline
from astropy.cosmology import FlatLambdaCDM
from astropy.table import Table
from halotools.mock_observables import ra_dec_z
try:
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    from scipy.interpolate import interp1d
    from scipy import polyfit, polyval
except ImportError:
    print("Error importing matplotib or scipy modules. It will not be possible to use plot_xi_comparison function.")


def calculate_selection_function(catalog_table=None, catalog_filename=None,
                                 H0=None, Om0=None,
                                 filename_selfunc=None):
    """
    Calculate selection function for synthetic galaxy sample.

    catalog_table: astropy.Table, optional
        Table holding simulated galaxy sample
    catalog_filename: str, optional
        path to csv file holding simulated sample. Should be provided if catalog_table is not.
    H0: float
        Hubble constant, in units of km/s/Mpc
    Om0: float
        matter density parameter today
    filename_selfunc: str, optional
        path of file to save selection function

    Returns:
        z, nz
        z: np.ndarray
            array with redshifts (mean of bin)
        nz: np.ndarray
            array with number of galaxies in corresponding redshift bin
    """
    if catalog_table is None and catalog_filename is None:
        raise ValueError("catalog_table or catalog_filename should be provided.")
    if catalog_table is None:
        catalog_table = Table.read(catalog_filename)
    cosmo = FlatLambdaCDM(H0=H0, Om0=Om0)

    X = np.c_[catalog_table["x"], catalog_table["y"], catalog_table["z"]]
    V = np.c_[catalog_table["vx"], catalog_table["vy"], catalog_table["vz"]]

    _, _, z = ra_dec_z(X, V, cosmo=cosmo)

    selfunc, z_bins = np.histogram(z)
    z_bins_mean = 0.5*(z_bins[:-1] + z_bins[1:])
    if filename_selfunc is not None:
        np.savetxt(filename_selfunc, np.c_[z_bins_mean, selfunc], delimiter=" ")

    return z_bins_mean, selfunc


def calculate_acf(hodpars, angles, pipeline_ini_file, env_vars=None, override_dict=None):
    """
    Calculates theoretical acf for hod parameters hodpars.

    Parameters:
        hodpars: dict
            describes hod parameters. Should have the keys sigmaM, massMin, alpha, mass0, mass1, fcentral
        angles: np.ndarray
            array containing angles to calc acf on.
        pipeline_ini_file: str
            path to ini file describing pipeline
        env_vars: dict
            environment variables used in ini file that should be specified. Optional.
        override_dict: dict or None
            dictionary with items on ini configuration file to replace. Keys are tuples (section, key) and
            and the values the corresponding values to replace.

    Returns:
        angle, acf, acf_1h, acf_2h, ndensity

        angle: np.ndarray
            array with angles in degrees
        acf: np.ndarray
            array with calculated values for acf
        acf_1h: np.ndarray
            array with calculated values for 1 halo acf
        acf_2h: np.ndarray
            array with calculated values for 2 halo acf
        ndensity: float
            galaxy volumetric density for given hodpars
    """

    if env_vars:
        for key in env_vars:
            os.environ[key] = env_vars[key]

    acf_temp = np.zeros_like(angles)
    acferr_temp = np.zeros_like(angles)
    acf_file = "./acf_temp.dat"
    np.savetxt(acf_file, np.c_[angles, acf_temp, acferr_temp])

    cov_file = ""

    if override_dict is None:
        override_dict = dict()

    override_dict_new = {
        ("runtime", "sampler"): "test",
        ("hmc", "save_acf"): "F",
        ("hmc", "save_xi"): "T",
        ("sample_files", "acf"): acf_file,
        ("sample_files", "cov"): cov_file,
    }

    for key in override_dict_new:
        override_dict[key] = override_dict_new[key]

    for key in hodpars:
        override_dict[("hod_parameters", key)] = str(hodpars[key])

    ini = Inifile(pipeline_ini_file, override=override_dict)
    pipeline = LikelihoodPipeline(ini, override=override_dict)

    data = pipeline.run_parameters(pipeline.parameters)

    ndensity = data["hod_parameters", "ndensity"]
    acf = data["acf_model", "acf"]
    acf_1h = data["acf_model", "acf_1h"]
    acf_2h = data["acf_model", "acf_2h"]
    angle = data["acf_model", "angle"]

    return angle, acf, acf_1h, acf_2h, ndensity


def calculate_xi(hodpars, pipeline_ini_file, env_vars=None, override_dict=None,
                 return_1h_components=False, return_ndensity=False):
    """
    Calculates theoretical xi(r) for hod parameters hodpars.

    Parameters:
        hodpars: dict
            describes hod parameters. Should have the keys sigmaM, massMin, alpha, mass0, mass1, fcentral
        pipeline_ini_file: str
            path to ini file describing pipeline
        env_vars: dict
            environment variables used in ini file that should be specified. Optional.
        override_dict: dict or None
            dictionary with items on ini configuration file to replace. Keys are tuples (section, key) and
            and the values the corresponding values to replace.
        return_1h_components: bool
            whether or not to return central-satellite and satellite-satellite components of 1 halo term.
            Defaults to False
        return_ndensity: bool
            whether or not to return galaxy density. Defaults to False.

    Returns:
        r, xi, xi_1h, xi_2h
        r, xi, xi_1h, xi_2h, xi_cs, xi_ss                (if return_1h_components is True)
        r, xi, xi_1h, xi_2h, ndensity                    (if return_ndensity is True)
        r, xi, xi_1h, xi_2h, xi_cs, xi_ss, ndensity      (if return_ndensity and return_1h_components are True)

        r: np.ndarray
            array with radial distances (in Mpc/h)
        xi: np.ndarray
            array with calculated values for spatial correlation function
        xi_1h: np.ndarray
            array with calculated values for 1 halo spatial correlation function
        xi_2h: np.ndarray
            array with calculated values for 2 halo spatial correlation function
        xi_cs: np.ndarray
            array with calculated values for central-satellite term of 1 halo spatial correlation function.
        xi_ss: np.ndarray
            array with calculated values for satellite-satellite term of 1 halo spatial correlation function.
        ndensity: float
            galaxy density for given hodpars.

    """
    if env_vars:
        for key in env_vars:
            os.environ[key] = env_vars[key]

    r = np.logspace(1e-3, 1, 30)
    xi_temp = np.zeros_like(r)
    xierr_temp = np.zeros_like(r)
    xi_file = "./xi_temp.dat"
    np.savetxt(xi_file, np.c_[r, xi_temp, xierr_temp])

    cov_file = ""

    if override_dict is None:
        override_dict = dict()

    override_dict_new = {
        ("runtime", "sampler"): "test",
        ("hmc", "save_acf"): "F",
        ("hmc", "save_xi"): "T",
        ("sample_files", "xi"): xi_file,
        ("sample_files", "cov"): cov_file,
    }

    for key in override_dict_new:
        override_dict[key] = override_dict_new[key]

    for key in hodpars:
        override_dict[("hod_parameters", key)] = str(hodpars[key])

    ini = Inifile(pipeline_ini_file, override=override_dict)
    pipeline = LikelihoodPipeline(ini, override=override_dict)

    data = pipeline.run_parameters(pipeline.parameters)

    r = data["xi_model", "r"]
    xi = data["xi_model", "xi"]
    xi_1h = data["xi_model", "xi_1h"]
    xi_2h = data["xi_model", "xi_2h"]

    return_tuple = r, xi, xi_1h, xi_2h

    if return_1h_components and not return_ndensity:
        xi_cs = data["xi_model", "xi_1h_cs"]
        xi_ss = data["xi_model", "xi_1h_ss"]
        return r, xi, xi_1h, xi_2h, xi_cs, xi_ss
    if return_ndensity:
        ndensity = data["hod_parameters", "ndensity"]
        if return_1h_components:
            return r, xi, xi_1h, xi_2h, xi_cs, xi_ss, ndensity
        else:
            return r, xi, xi_1h, xi_2h, ndensity

    return return_tuple


def plot_xi_comparison(hodpars, pipeline_ini_file, env_vars, override_dict,
                       r_compare, xi_compare, xierr_compare=None,
                       analytic_components=False,
                       xi1h_compare=None, xi2h_compare=None,
                       analytic_label="analytical", other_label="other",
                       **kwargs
                       ):
    """
    Makes plot comparing xi calculated by cosmosis with another calculation.

    Parameters:
        hodpars: dict
            describes hod parameters. Should have the keys sigmaM, massMin, alpha, mass0, mass1, fcentral
        pipeline_ini_file: str
            path to ini file describing pipeline
        env_vars: dict
            environment variables used in ini file that should be specified. Optional.
        override_dict: dict or None
            dictionary with items on ini configuration file to replace. Keys are tuples (section, key) and
            and the values the corresponding values to replace.
        r_compare: np.ndarray
            positions, in Mpc/h, of correlation function measurements/calculations to compare to
        xi_compare: np.ndarray
            correlation function measurements/calculations to compare to
        xierr_compare: np.ndarray
            error in correlation function
        analytic_components: bool
            whether to plot analytic one and two halo components
        xi1h_compare: np.ndarray
            1 halo component of correlation function to compare to
        xi2h_compare: np.ndarray
            2 halo component of correlation function to compare to
        analytic_label: str
            label of analytical code
        other_label: str
            label of other calculation we are comparing to
        **kwargs: dict
            remaining keyword arguments are used when creating matplotlib figure.

    Returns:
        fig, (ax1, ax2, ax3)
        fig: matplotlib.figure.Figure
            matplotlib figure object holding plot
        ax1, ax2, ax3: matplotlib.axes._subplots.AxesSubplot
            matplotlib axes of subplots in figure

    """
    if "figsize" not in kwargs:
        kwargs["figsize"] = (15, 7)
    gs = gridspec.GridSpec(2, 2, height_ratios=[3, 1])
    fig = plt.figure(**kwargs)

    ax3 = plt.subplot(gs[1, 0])
    ax1 = fig.add_subplot(gs[0, 0], sharex=ax3)
    ax2 = fig.add_subplot(gs[:, 1])

    r_cosmosis, xi_cosmosis, xi_1halo, xi_2halo = calculate_xi(hodpars=hodpars,
                                                               pipeline_ini_file=pipeline_ini_file,
                                                               env_vars=env_vars,
                                                               override_dict=override_dict,
                                                               return_ndensity=False)

    ax1.loglog()
    ax1.set_ylabel(r"$\xi(r)$")
    ax1.set_xlim((1e-2, 1e2))
    line, = ax1.plot(r_cosmosis, xi_cosmosis, label=analytic_label)
    analytic_color = line.get_color()
    if analytic_components:
        ax1.plot(r_cosmosis, xi_1halo, ls="dashed", color=analytic_color, label=analytic_label + " 1 halo")
        ax1.plot(r_cosmosis, xi_2halo, ls="dotted", color=analytic_color, label=analytic_label + " 2 halo")

    other_line, _, _ = ax1.errorbar(r_compare, xi_compare, xierr_compare, label=other_label)
    other_color = other_line.get_color()
    if xi1h_compare:
        ax1.plot(r_compare, xi1h_compare, ls="dashed", color=other_color, label=other_label + " 1 halo")
    if xi2h_compare:
        ax1.plot(r_compare, xi2h_compare, ls="dotted", color=other_color, label=other_label + " 2 halo")
    ax1.legend();


    ps = polyfit(np.log(r_compare), np.log(xi_compare), 1)
    def y_power_law(x):
        return np.exp(polyval(ps, np.log(x)))
    def delta(x, y):
        power_y = y_power_law(x)
        return (y - power_y)/power_y

    ax2.set_xlabel(r"$r$ [Mpc/h]")
    ax2.set_ylabel(r"$\frac{\xi(r) - C r^\alpha}{Cr^\alpha}$", fontsize=14)
    ax2.set_xscale("log")
    ax2.set_xlim((1e-2, 1e2))
    ax2.set_ylim((-2, 2))
    ax2.plot(r_cosmosis, delta(r_cosmosis, xi_cosmosis), label=analytic_label, color=analytic_color)
    if analytic_components:
        ax2.plot(r_cosmosis, delta(r_cosmosis, xi_1halo), ls="dashed", label=analytic_label + " 1 halo", color=analytic_color)
        ax2.plot(r_cosmosis, delta(r_cosmosis, xi_2halo), ls="dotted", label=analytic_label + " 2 halo", color=analytic_color)
    ax2.plot(r_compare, delta(r_compare, xi_compare), label=other_label, color=other_color)
    if xi1h_compare:
        ax2.plot(r_compare, delta(r_compare, xi1h_compare), ls="dashed", label=other_label + " 1 halo", color=other_color)
    if xi2h_compare:
        ax2.plot(r_compare, delta(r_compare, xi2h_compare), ls="dotted", label=other_label + " 2 halo", color=other_color)
    ax2.legend();


    interp_cosmosis = interp1d(r_cosmosis, xi_cosmosis)
    if xierr_compare is not None:
        interp_err = interp1d(r_compare, xierr_compare/xi_compare)

    mask = (r_cosmosis > r_compare.min()) * (r_cosmosis < r_compare.max())
    r_cosmosis_rest = r_cosmosis[mask]
    xi_cosmosis_rest = xi_cosmosis[mask]
    relative_dev = (xi_compare / interp_cosmosis(r_compare) - 1) * 100
    if xierr_compare is not None:
        ax3.errorbar(r_compare, relative_dev, interp_err(r_compare) * relative_dev, label="deviation relative to analytical calculation", color=other_color)
    else:
        ax3.plot(r_compare, relative_dev, label="deviation relative to analytical calculation", color=other_color)
    ax3.axhline(0, ls="dotted", color="k", alpha=1.0)
    ax3.axhline(10, ls="dashed", color="k", alpha=0.2)
    ax3.axhline(-10, ls="dashed", color="k", alpha=0.2)
    ax3.axhline(25, ls="dashed", color="k", alpha=0.3)
    ax3.axhline(-25, ls="dashed", color="k", alpha=0.3)
    ax3.axhline(50, ls="dashed", color="k", alpha=0.5)
    ax3.axhline(-50, ls="dashed", color="k", alpha=0.5)
    ax3.set_ylim((-60, 60))
    ax3.set_xlabel(r"$r$ [Mpc/h]")
    ax3.set_ylabel(r"$\frac{\Delta \xi(r)}{\xi(r)}$ [%]")

    return fig, (ax1, ax2, ax3)

